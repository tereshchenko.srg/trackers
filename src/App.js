import React, { Component } from 'react';
import {Route, Switch, Redirect} from 'react-router-dom'
import 'App.css';

import Login from 'containers/Login'
import Trackers from "containers/Map"

class App extends Component {
  render() {
    return (
      <div className="App">
          <Switch>
              <Route exact path="/" component={Login}/>
              <Route path={`/map`} component={Trackers}/>

             {/* <Route path="*" component={NotFound} status={404} />*/}
               <Redirect to={"/"}/>
          </Switch>
      </div>
    );
  }
}

export default App;
