import React from 'react'
import ReactDOM from 'react-dom'
import {createStore, applyMiddleware} from 'redux'
import {Provider} from 'react-redux'
import thunkMiddleware from 'redux-thunk'
import sessionMiddleware from 'store/middleware/session'
import {composeWithDevTools} from 'redux-devtools-extension'
import rootReducer from 'store'
import './index.css'
import App from './App'
import * as serviceWorker from './serviceWorker'
//import createHistory from 'history/createBrowserHistory'
import { createBrowserHistory } from 'history';
import {ConnectedRouter, routerMiddleware} from 'react-router-redux'
import {Route} from 'react-router-dom'

//const history = createHistory();
const history = createBrowserHistory();

const middleware = [
    routerMiddleware(history),
    thunkMiddleware,
    sessionMiddleware,
];

const enhancer = composeWithDevTools(applyMiddleware(...middleware));

let sessionState = null;

try {
    sessionState = JSON.parse(localStorage.getItem('session'))
} catch (err) {
    console.log(err)
}

const persistedState = sessionState ? sessionState : {};

export const store = createStore(
    rootReducer,
    {session: persistedState},
    enhancer
);

ReactDOM.render((
    <Provider store={store} key={Math.random()}>
        <ConnectedRouter history={history}>
            <Route path="/" component={App}/>
        </ConnectedRouter>
    </Provider>
), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
