/*const setUserToken = () => {
    const user = JSON.parse(localStorage.getItem('session'));
    if (user) {
        return user.token
    } else {
        return false
    }
};*/

export const fetchTrackersApi = () => {

    return fetch(`/api/v1/trackers`, {
        method: "GET",
        headers: new Headers({
            "Content-Type": "application/json",
            //"Authorization": `JWT ${setUserToken()}`
        })
    }).then(res => res.json())
};


export const fetchTrackerLastPointApi = (tracker_id) => {

    return fetch(`/api/v1/trackers/${tracker_id}/last_point`, {
        method: "GET",
        headers: new Headers({
            "Content-Type": "application/json",
        })
    })
};


export const fetchTrackerDataApi = (trackerId, from, till) => {
        return fetch(`/api/v1/trackers/${trackerId}/data?date_from=${from}&date_till=${till}`, {
            method: "GET",
            headers: new Headers({
                "Content-Type": "application/json",
                //"Authorization": `JWT ${setUserToken()}`
            })
        })

};

export const fetchLogInApi = (username, password) => (
    fetch(`/api/v1/users/me/login`, {
        method: "POST",
        headers: new Headers({
            "Content-Type": "application/json",
        }),
        body: JSON.stringify({username, password})
    })
        .then(res => {
            if (
                res.status === 200 ||
                res.status === 201 ||
                res.status === 400 ||
                res.status === 401
            ) {
                return res.json();
            }
            throw new Error();
        })
);

/*export const fetchUserSignUpApi = (first_name, email, password) => (
    fetch(`/api/v1/registration/`, {
        method: "POST",
        headers: new Headers({
            "Content-Type": "application/json",
        }),
        body: JSON.stringify({first_name, email, password})
    })
        .then(res => res.json())
);*/

/*export const isValidTokenAPI = (token) => (
    fetch(`/api/v1/users/email-validate/${token}`, {
        method: "POST",
        headers: new Headers({
            "Content-Type": "application/json",
        }),
    })
        .then(res => res.json())
);*/


/*export const sendProfileInfo = (data) => (
    fetch('/api/v1/users/me/profile/personal_info', {
        method: 'POST',
        headers: new Headers({
            "Content-Type": "application/json",
            //"Authorization": `JWT ${setUserToken()}`
        }),
        body: JSON.stringify(data)
    })
        .then(res => res.json())
);*/

export const sendLogoutApi = () => (
    fetch('/api/v1/users/me/logout')
);

/*export const sendPasswordRecoveryRequestApi = (data) => (
    fetch('/api/v1/users/reset-password', {
        method: 'POST',
        headers: new Headers({
            "Content-Type": "application/json",
            //"Authorization": `JWT ${setUserToken()}`
        }),
        body: JSON.stringify(data)
    })
        .then(res => res.json())
);*/

/*export const getPasswordRecoveryConfirmationApi = (token) => (
    fetch(`/api/v1/users/reset-password/${token}`, {
        headers: new Headers({
            "Content-Type": "application/json",
            //"Authorization": `JWT ${setUserToken()}`
        })
    })
        .then(res => res.json())
);*/

/*export const getPasswordRecoveryChangeApi = (token, data) => (
    fetch(`/api/v1/users/reset-password/${token}`, {
        method: 'POST',
        headers: new Headers({
            "Content-Type": "application/json",
            //"Authorization": `JWT ${setUserToken()}`
        }),
        body: JSON.stringify(data)
    })
        .then(res => res.json())
);*/
