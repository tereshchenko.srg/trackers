/*global google*/
import React, {Fragment} from 'react'
import styled from "styled-components";
import {compose, withProps, withStateHandlers} from 'recompose'
import {withScriptjs, withGoogleMap, GoogleMap, Polyline, Marker, InfoWindow} from 'react-google-maps'

import normalizeTime from 'utils/normalize/normalizeTime';

const myMapStyles = require("./myMapStyles.json");

const parkingIcon = require("./parking_mark.svg");

const icon_car_top = "M29.395,0H17.636c-3.117,0-5.643,3.467-5.643,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759 c3.116,0,5.644-2.527,5.644-5.644V6.584C35.037,3.467,32.511,0,29.395,0z M34.05,14.188v11.665l-2.729,0.351v-4.806L34.05,14.188z M32.618,10.773c-1.016,3.9-2.219,8.51-2.219,8.51H16.631l-2.222-8.51C14.41,10.773,23.293,7.755,32.618,10.773z M15.741,21.713 v4.492l-2.73-0.349V14.502L15.741,21.713z M13.011,37.938V27.579l2.73,0.343v8.196L13.011,37.938z M14.568,40.882l2.218-3.336 h13.771l2.219,3.336H14.568z M31.321,35.805v-7.872l2.729-0.355v10.048L31.321,35.805z";

const DetailsList = styled.ul`
/*background: black;*/
margin: 0;
padding: 0;
padding-left: 0;

& > li:first-child {
justify-content: center;
& > span {
font-size: 15px;
font-weight: 400;
}
}

`;

const ListItem = styled.li`
min-width: 175px;
display: flex;
justify-content: flex-start;

& > span:first-child {
min-width: 75px;
text-align: left;
text-transform: capitalize;
}
`;

const MyMarker = styled(Marker)`
    transform: rotateZ(90deg);
    background: red;
`;


class MyMapComponent extends React.Component {

    render() {
        const PolylineComponent = compose(
            withProps({
                googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyBdmhUVpQfmGk7KebkXHXDpsJ4zMCUWAzQ&v=3.exp&libraries=geometry,drawing,places",
                loadingElement: <div style={{height: `400px`}}/>,
                containerElement: <div style={{width: `100%`}}/>,
                mapElement: <div style={{height: `75vh`, width: `100%`}}/>,
                activeTracker: this.props.activeTracker,
                data: this.props.data,
                parking: this.props.parking,
                lastPoint: this.props.lastPoint,
                icon: this.props.icon,
            }),
            withScriptjs,
            withGoogleMap,
            withStateHandlers(
                () => ({
                    isOpen: false,
                    activeId: null
                }), {
                    onToggleOpen: ({isOpen, activeId}) => (id) => ({
                        isOpen: activeId === null ? !isOpen : activeId === id ? !isOpen : isOpen,
                        activeId: activeId === id ? null : id,
                    })
                }),
        )(({activeTracker, data, lastPoint, icon, parking, ...props}) =>
            <GoogleMap
                defaultCenter={{
                    lat: data.length >= 2 ? data[Math.round(data.length / 2)].latitude : lastPoint.latitude || 50.452289,
                    lng: data.length >= 2 ? data[Math.round(data.length / 2)].longitude : lastPoint.longitude || 30.523718
                }}
                defaultZoom={10}
                defaultOptions={{
                    styles: myMapStyles,
                    zoomControl: false,
                    mapTypeControl: false,
                    scaleControl: false,
                    streetViewControl: false,
                    rotateControl: false,
                    fullscreenControl: false
                }}
            >
                {
                    data[0] ?
                        <Fragment>
                            <Polyline
                                path={[...data.sort(function (a, b) {
                                    let dateA = new Date(a.date), dateB = new Date(b.date)
                                    return dateA - dateB
                                }).map(point => ({
                                    lat: point.latitude,
                                    lng: point.longitude
                                }))
                                ]}
                                geodesic={true}
                                options={{
                                    strokeColor: "#E00051",
                                    strokeOpacity: 1,
                                    strokeWeight: 2,
                                    icons: [
                                        {
                                            icon: '',
                                            offset: "0",
                                            repeat: "20px"
                                        }
                                    ]
                                }}
                            />
                            <Marker
                                position={{
                                    lat: data[data.length - 1].latitude,
                                    lng: data[data.length - 1].longitude
                                }}
                                icon={{
                                    path: icon_car_top,
                                    fillColor: '#E00051',
                                    fillOpacity: 1,
                                    scale: 1,
                                    rotation: lastPoint.course,
                                    anchor: new google.maps.Point(7, 25)
                                }}
                                onClick={() => props.onToggleOpen(data[data.length - 1].id)}
                            >
                                {props.isOpen && props.activeId === data[data.length - 1].id &&
                                <InfoWindow onCloseClick={props.onToggleOpen}>
                                    <DetailsList>
                                        <ListItem>
                                            <span>{activeTracker.name}</span>
                                        </ListItem>
                                        <ListItem>
                                            <span>state.id</span>
                                            <span>{props.activeId}</span>
                                        </ListItem>
                                        <ListItem>
                                            <span>IMEI</span>
                                            <span>{activeTracker.imei}</span>
                                        </ListItem>
                                        <ListItem>
                                            <span>speed</span>
                                            <span>{`${data[data.length - 1].speed} km/h`}</span>
                                        </ListItem>
                                    </DetailsList>
                                </InfoWindow>}
                            </Marker>

                            {parking[0] && parking.map(el => (
                                <Marker key={`${el.lon}_${el.time}`}
                                        position={{
                                            lat: el.lat,
                                            lng: el.lon
                                        }}
                                        icon={{
                                            url: parkingIcon,
                                            anchor: new google.maps.Point(0, 16)
                                        }}
                                        onClick={() => props.onToggleOpen(el.id)}
                                >
                                    {props.isOpen && props.activeId === el.id &&
                                    <InfoWindow onCloseClick={props.onToggleOpen}>
                                        <DetailsList>
                                            <ListItem>
                                                <span>{activeTracker.name}</span>
                                            </ListItem>
                                            <ListItem>
                                                <span>IMEI</span>
                                                <span>{activeTracker.imei}</span>
                                            </ListItem>
                                            <ListItem>
                                                <span>start</span>
                                                <span>{el.start}</span>
                                            </ListItem>
                                            <ListItem>
                                                <span>end</span>
                                                <span>{el.stop}</span>
                                            </ListItem>
                                            <ListItem>
                                                <span>total time</span>
                                                <span>{normalizeTime(el.stop, el.start)}</span>
                                            </ListItem>
                                        </DetailsList>
                                    </InfoWindow>}
                                </Marker>
                            ))

                            }

                        </Fragment>
                        : lastPoint.latitude && <MyMarker
                        position={{
                            lat: lastPoint.latitude,
                            lng: lastPoint.longitude
                        }}
                        icon={
                            {
                                path: icon_car_top,
                                fillColor: '#E00051',
                                fillOpacity: 1,
                                scale: 1,
                                rotation: lastPoint.course,
                                anchor: new google.maps.Point(7, 25)
                            }
                        }
                        onClick={() => props.onToggleOpen(lastPoint.id)}
                    >
                        {props.isOpen && props.activeId === lastPoint.id &&
                        <InfoWindow onCloseClick={props.onToggleOpen}>
                            <DetailsList>
                                <ListItem>
                                    <span>{activeTracker.name}</span>
                                </ListItem>
                                <ListItem>
                                    <span>IMEI</span>
                                    <span>{activeTracker.imei}</span>
                                </ListItem>
                                <ListItem>
                                    <span>date</span>
                                    <span>{lastPoint.date}</span>
                                </ListItem>
                                <ListItem>
                                    <span>speed</span>
                                    <span>{`${lastPoint.speed} km/h`}</span>
                                </ListItem>
                            </DetailsList>
                        </InfoWindow>}
                    </MyMarker>
                }

            </GoogleMap>
        );

        //console.log('>>>>>>>>>>>>>>> lastPoint', this.props.lastPoint)
        //console.log('>>>>>>>>>>>>>>> data', this.props.data)
        console.log('>>>>>>>>>>>>>>> parking', this.props.parking)

        this.props.parking[0] && console.log('>>>>>>>>>>>>>>> parking Time', normalizeTime(this.props.parking[0].stop, this.props.parking[0].start))

        return (
            <PolylineComponent/>
        )
    }
}

export default MyMapComponent
