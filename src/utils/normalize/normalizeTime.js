let moment = require('moment');

const normalizeTime = (dateEnd, dateStart) => {

    let start = moment(dateStart, "HH:mm:ss DD-MM-YYYY").valueOf();
    let end = moment(dateEnd, "HH:mm:ss DD-MM-YYYY").valueOf();

    let time = (end - start) / 1000;

    if (time <= 59) {
        let h = '00';
        let m = '00';
        let s = time;
        return  `${h}:${m}:${s}`
    } else if (time <= 3599) {
        let h = '00';
        let m = Math.floor(time / 60);
        let s = time - (m * 60);
        return `${h}:${m < 10 ? `0${m}` : m}:${s < 10 ? `0${s}` : s}`
    } else if (time <= 86399) {
        let h = Math.floor(time / 60 / 60);
        let m = Math.floor(time / 60) - (h * 60);
        let s = time - (h * 60 * 60) - (m * 60);
        return `${h < 10 ? `0${h}` : h}:${m < 10 ? `0${m}` : m}:${s < 10 ? `0${s}` : s}`
    }else {
        let d = Math.floor(time / 60 / 60 / 24);
        let h = Math.floor(time / 60 / 60) - (d * 24);
        let m = Math.floor(time / 60) - (d * 24 * 60) - (h * 60);
        let s = time - (d * 24 * 60 * 60) - (h * 60 * 60) - (m * 60);
        return `${d}day ${h < 10 ? `0${h}` : h}:${m < 10 ? `0${m}` : m}:${s < 10 ? `0${s}` : s}`
    }
};

export default normalizeTime
