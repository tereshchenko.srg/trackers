import {combineReducers} from 'redux';
import {
    LOG_IN_REQUEST,
    LOG_IN_SUCCESS,
    LOG_IN_FAILURE,
    LOG_OUT,
    USER_INFO_SUCCESS,
    IS_VALID_TOKEN_SUCCESS,
    IS_VALID_TOKEN_FAIL,
    CLEAR_SIGN_UP,
    SIGN_UP_SUCCESS,
    SIGN_UP_FAIL,
    PASSWORD_RECOVERY_REQUEST_SUCCESS,
    PASSWORD_RECOVERY_REQUEST_FAIL,
    PASSWORD_RECOVERY_CHANGE_SUCCESS,
    PASSWORD_RECOVERY_CHANGE_FAIL,
    PASSWORD_RECOVERY_CONFIRMATION_SUCCESS,
    PASSWORD_RECOVERY_CONFIRMATION_FAIL,
    CLEAR_PASSWORD_RECOVERY
} from '../actions/types';

function user(state = {
    first_name: null,
    last_name: null,
    email: null,
    username: null,
    organization: null,

}, {type, payload}) {
    switch (type) {
        case USER_INFO_SUCCESS:
            return {...payload};

        case LOG_OUT:
            return {
                first_name: null,
                last_name: null,
                email: null,
                username: null,
                organization: null,
            };

        default:
            return state;
    }
}

function authenticated(state = false, {type, payload}) {
    switch (type) {
        /* case LOG_IN_SUCCESS:*/
        case USER_INFO_SUCCESS:
            return true;

        case LOG_OUT:
            return false;

        default:
            return state;
    }
}

function authenticating(state = false, {type, payload}) {
    switch (type) {
        case LOG_IN_REQUEST:
            return true;

        case LOG_IN_SUCCESS:
        case LOG_IN_FAILURE:
            return false;

        default:
            return state;
    }
}

function token(state = null, {type, payload}) {
    switch (type) {
        case LOG_IN_SUCCESS:
            return payload.token;

        case LOG_OUT:
            return null;

        default:
            return state;
    }
}

function error(state = {error: null}, {type, payload}) {
    switch (type) {
        case LOG_IN_REQUEST:
        case LOG_IN_SUCCESS:
        case LOG_OUT:
            return {error: null};

        case LOG_IN_FAILURE:
            return {...payload};
        default:
            return state;
    }
}


function signUp(state = {formError: {}, message: null, warningMessages: null, }, {type, payload}) {
    switch (type) {
        case IS_VALID_TOKEN_SUCCESS:
        case SIGN_UP_SUCCESS:
            return {...state, message: payload};

        case IS_VALID_TOKEN_FAIL:
            return {...state, warningMessages: payload};

        case SIGN_UP_FAIL:
            return {...state, formError: {...payload}};

        case CLEAR_SIGN_UP:
            return {message: null, warningMessages: null, formError: {}};

        default:
            return state;
    }
}

function forgotPassword(state = {requestError: null, changeError: null, requestMessage: null, changeMessage: null, warningMessages: null}, {type, payload}) {
    switch (type) {
        case PASSWORD_RECOVERY_REQUEST_SUCCESS:
        case PASSWORD_RECOVERY_CONFIRMATION_SUCCESS:
            return {...state, requestMessage: payload.message};

        case PASSWORD_RECOVERY_CHANGE_SUCCESS:
            return {...state, changeMessage: payload.message}

        case PASSWORD_RECOVERY_CONFIRMATION_FAIL:
            return {...state, warningMessages: payload};

        case PASSWORD_RECOVERY_REQUEST_FAIL:
            return {...state, requestError: payload.error};
        case PASSWORD_RECOVERY_CHANGE_FAIL:
            return {...state, changeError: payload.error};

        case CLEAR_PASSWORD_RECOVERY:
            return {requestError: null, changeError: null, requestMessage: null, changeMessage: null, warningMessages: null};

        default:
            return state;
    }
}

export default combineReducers({
    user,
    authenticated,
    authenticating,
    token,
    error,
    signUp,
    forgotPassword
});
