import {combineReducers} from 'redux'
import {routerReducer} from 'react-router-redux'
import session from './reducers/session';

const initialState = {
    isLoading: false,
    result: [],
    activeTracker: {},
    activeFilter: {},
    activeTrackerData: [],
    activeTrackerParking: [],
    trackDetails: {},
    lastPoint: {},
}
const tracker = (state = {...initialState}, action) => {
    switch (action.type) {
        case 'LOAD_TRACKER':
            return {
                ...state,
                result: [],
                isLoading: true
            }
        case 'FETCH_TRACKER':
            return {
                ...state, ...action.result,
                isLoading: false
            }
        case 'CHANGE_TRACKER':
            return {
                ...state,
                activeTracker: {...action.tracker},
                activeTrackerData: [],
                activeTrackerParking: [],
                trackDetails: {},
                lastPoint: {},
            };
        case 'FETCH_TRACKER_DATA':
            return {
                ...state,
                activeTrackerData: [...action.result.result],
                activeTrackerParking: [...action.result.parking],
                trackDetails: {...action.result.track_details}
            };
        case 'FETCH_TRACKER_LAST_POINT':
            return {
                ...state, lastPoint: {...action.result.result}
            };
        default:
            return state
    }
}

const appReducer = combineReducers({
    session,
    tracker,
    router: routerReducer,
})

const rootReducer = (state, action) => {
    return appReducer(state, action)
}

export default rootReducer
