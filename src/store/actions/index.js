import {
    fetchTrackersApi,
    fetchTrackerLastPointApi,
    fetchTrackerDataApi,
    fetchLogInApi,

    sendLogoutApi,
} from 'api/'
import {
    LOG_IN_REQUEST,
    LOG_IN_FAILURE,
    LOG_OUT,
    USER_INFO_SUCCESS,
    CLEAR_SIGN_UP,
} from '../actions/types'


/*export const fetchIndex = () => {
    return async (dispatch) => {
        dispatch({
            type: 'LOAD_INDEX'
        })
        fetchIndexApi()
            .then(res => {
                if (res.status === 401 ) {
                    dispatch(logUserOut())
                } else if(res.status === 200) {
                    return res.json()
                }
            })
            .then(result => {
                if (result) {
                    return dispatch({
                        type: 'FETCH_INDEX', result
                    })
                }
            })
    }
};*/

export const fetchTrackers = () => {
    return async (dispatch) => {
        //dispatch(loadTracker())
        fetchTrackersApi()
            .then(result => {
                dispatch({
                    type: 'FETCH_TRACKER', result
                })
            })
    }
};

export const loadTracker = () => ({
    type: 'LOAD_TRACKER'
})

export const changeTracker = tracker => ({
    type: 'CHANGE_TRACKER',
    tracker,
});

export const fetchTrackerLastPoint = (trackerId) => {
    return async (dispatch) => {
        fetchTrackerLastPointApi(trackerId)
            .then(res => {
                if (res.status === 401) {
                    throw new Error(401)
                }
                return res.json()
            })
            .then(result => {
                dispatch({
                    type: 'FETCH_TRACKER_LAST_POINT',
                    result
                })
            })
            .catch(e => {
                if (e === 401) {
                    dispatch(logUserOut())
                }
            })
    }
};

export const fetchTrackerData = (trackerId, from, till) => {
    return async (dispatch) => {
        fetchTrackerDataApi(trackerId, from, till)
            .then(res => {
                if (res.status === 401) {
                    throw new Error(401)
                }
                return res.json()
            })
            .then(result => {
                dispatch({
                    type: 'FETCH_TRACKER_DATA',
                    result
                })
            })
            .catch(e => {
                if (e === 401) {
                    dispatch(logUserOut())
                }
            })
    }
};

export const loadTrackerData = () => ({
    type: 'LOAD_TRACKER_DATA'
});

export const logUserIn = ({username, password}) => dispatch => {
    //dispatch({type: LOG_IN_REQUEST});
    fetchLogInApi(username, password)
        .then(data => {
            if(data.error){
                dispatch({type: LOG_IN_FAILURE, payload: data})
            } else {
                dispatch({
                    type: USER_INFO_SUCCESS,
                    payload: {...data}
                })
            }

        })
        .then(a => dispatch({type: 'SAVE_SESSION'}))
        /*.then(res => {
                if (res.token) {
                    dispatch({type: LOG_IN_SUCCESS, payload: res});
                    fetchUserInfoApi(username, res.token)
                        .then(data => {
                            dispatch({
                                type: USER_INFO_SUCCESS,
                                payload: {...data, username}
                            })
                        })
                        .then(a => dispatch({type: 'SAVE_SESSION'}))
                } else {
                    dispatch({type: LOG_IN_FAILURE, payload: res})
                }
            }
        )*/
        .catch(err => console.log('err: ', err))
};

export const profileUser = (email, token) => dispatch => {
    dispatch({type: LOG_IN_REQUEST});
    /*fetchUserInfoApi(email, token)
        .then(res => dispatch({
                type: USER_INFO_SUCCESS,
                payload: {...res, email}
            })
        )
        .then(a => {
            dispatch({type: LOG_IN_SUCCESS, payload: {token}})})
        .then(b => dispatch({type: 'SAVE_SESSION'}))
        .catch(err => console.log('err: ', err))*/
};

/*export const allProfileUser = (user_id, token) => dispatch => {
    dispatch({type: LOG_IN_REQUEST});
    fetchUserProfileApi(user_id)
        .then(res => dispatch({
            type: USER_INFO_SUCCESS,
            payload: {...res}
        }))
        .then(a => {
            dispatch({type: LOG_IN_SUCCESS, payload: {token}})})
        .then(b => dispatch({type: 'SAVE_SESSION'}))
        .catch(err => console.log('err: ', err))
};*/

export const logUserOut = () => async dispatch => {
    dispatch({type: LOG_OUT});
    dispatch({type: 'SAVE_SESSION'});
    sendLogoutApi()
        .then(res => {
            window.location.reload()
            /*dispatch(fetchIndex(userLang, lang))*/
        })
};

/*export const signUserUp = ({first_name, email, password}) => dispatch => {
    fetchUserSignUpApi(first_name, email, password)
        .then(res => {
                if (res.message) {
                    dispatch({type: SIGN_UP_SUCCESS, payload: res.message})
                } else {
                    dispatch({type: SIGN_UP_FAIL, payload: res})
                }
            }
        ).catch(err => {
        console.log('err: ', err);
    })
};*/

/*export const isValidToken = (token) => dispatch => {
    isValidTokenAPI(token)
        .then(res => {
            if (res.message) {
                dispatch({type: IS_VALID_TOKEN_SUCCESS, payload: res.message})
            } else if (res.error) {
                dispatch({type: IS_VALID_TOKEN_FAIL, payload: res.error})
            }
        })
        .catch(err => {
            console.log('err: ', err)
        })
};*/

export const clearSignUp = () => dispatch => {
    dispatch({type: CLEAR_SIGN_UP});
};
