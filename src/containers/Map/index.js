import React, {Component} from 'react'
import {compose} from 'redux';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {withStyles} from "@material-ui/core/styles";

import {fetchTrackers, logUserOut, changeTracker, fetchTrackerData, fetchTrackerLastPoint} from 'store/actions';
import styled from "styled-components";

import MyMapComponent from 'components/Map';

import Button from "@material-ui/core/Button";
import TextField from '@material-ui/core/TextField';

let moment = require('moment');

const exit = require("./exit.svg");
const down = require("./down.svg");
const view = require("./view.svg");
//const hide = require("./hide.svg");
const parking = require("./parking.svg");
const parking_active = require("./parking_active.svg");
const diagram = require("./diagram.svg");
const route = require("./route.svg");
const route_active = require("./route_active.svg");
const car_top = require("./car_top.png");
const passenger_car = require("./car.svg");
const truck = require("./truck.svg");
const ship = require("./cargo-ship.svg");

const icon = {
    passenger_car: passenger_car,
    truck: truck,
    ship: ship
}

const iconTop = {
    passenger_car: car_top,
    truck: car_top,
    ship: car_top
}

const styles = {
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'center',
    },
    textField: {
        marginLeft: 10,
        marginRight: 10,
        width: 200,
        '& > label': {
            color: '#555555',
        },
        '& > div > input': {
            color: '#555555',
        },
    },
};

const SmallButton = styled.button`
border: none;
border-radius: 2px;
padding: 2px;
background: inherit;
cursor: pointer;
outline: none;
& > img {
width: 20px;
}

& > object {
width: 20px;
height: 20px;
}
&:hover {
background: rgba(224,0,81,.2);
}
`;

const ButtonLogOut = styled(Button)`
padding: 0!important;
min-width: 40px!important;
& > span:hover > img {
fill: rgba(224,0,81,.2);
/*background: rgba(224,0,81,.2);*/
}
& > span > img {
width: 40px;
}
`;

const Wrapper = styled.div`
position: relative;
width: 100%;
height: 100vh;
background: black;
`;

const Header = styled.header`
position: absolute;
padding: 0 20px;
top: 10px;
left: 24%;
width: 70%;
border-radius: 5px;
z-index: 2;
background: rgba(0,0,0,.9);
box-shadow: 0px 0px 50px 1px #333333;
opacity: 1;
transition: 1s;

& > div {
height: 75px;
position: relative;
display: flex;
align-items: center;
justify-content: space-between;

& > span {
line-height: 1;
color: #555555;
}
}

&.hidden {
top: -65px;
opacity: .5;
}

`;

const ButtonVisibility = styled.div`
position: absolute;
bottom: 65px;
left: 50%;
transform: translate(-50%,-50%);
width: 20px
height: 20px;
border-radius: 50%;
cursor: pointer;
transition: 1s;
transform: rotateZ(180deg);

& > img {
width: 20px
height: 20px;
}

&.hidden {
bottom: -10px;
transform: rotateZ(0deg);
}

`;

const Container = styled.div`
min-height: 100vh;
margin: 0 auto;
display: flex;
align-items: center;
justify-content: center;
`;
const SideBar = styled.div`
display: flex;
flex-direction: column;
min-height: 100vh;
width: 20%;
align-items: start;
justify-content: space-between;;
border-right: 1px solid #555555;
`;
const Main = styled.main`
min-height: 100vh;
width: 80%;

& > div > div > div > div {
background: #000000;
}
`;

const TrackerList = styled.ul`
padding: 0;
width: 100%;
& > li {
display: flex;
justify-content: space-between;
align-items: center;
list-style: none;
line-height: 30px;
color: #555555;
text-align: left;
padding: 0px 10px;

&:hover {
color: #333333;
cursor: pointer;
}

&.active {
color: #333333;
}

& > img {
width: 30px;
height: auto;
}

}
`;

const TrackerDetailsList = styled.ul`
padding: 0;
width: 100%;
& > li {
display: flex;
justify-content: space-between;
align-items: center;
list-style: none;
color: #555555;
text-align: left;
line-height: 30px;
padding: 0 10px;

&.active {
color: #333333;
}

& > img {
width: 30px;
height: auto;
}

}
`;

const TrackerDetails = styled.div`
position: relative;
min-height: 25vh;
width: 100%
text-align: left;

& > span {
padding-left: 10px; 
color: #555555;
&::after {
content: '';
display: block;
position: absolute;
top: 34px;
right: 0px;
width: 50%;
height: 1px;
background: #555555;
}
}
`;

const BlockButtons = styled.div`
padding: 0 10px;
display: flex;
justify-content: flex-end;
align-items: center;

& > button {
margin-left: 5px;
}
`;


class Trackers extends Component {
    state = {
        dateFrom: moment().startOf('month').format().slice(0, 16),
        dateTill: moment().format().slice(0, 16),
        isVisibleHeader: true,
        isVisibleParking: true,
    };


    componentDidMount() {
        this.props.fetchTrackers();
        this.checkAuthentication()
    }

    componentDidUpdate() {
        this.checkAuthentication()
    }

    checkAuthentication = () => {
        if (!this.props.authenticated) {
            this.props.history.push({pathname: `/`})
        }
    };

    handleLogOut = () => {
        const {logUserOut} = this.props;
        logUserOut();
    };

    handleInputChange = ({target}) => {
        const {value, id} = target;
        this.setState({[id]: value});
    };

    onSubmit = e => {
        e.preventDefault();
        const {dateFrom, dateTill} = this.state;
        const {tracker} = this.props;

        if (dateFrom === null || dateTill === null || tracker.activeTracker === '') return;

        this.props.fetchTrackerData(tracker.activeTracker.id, dateFrom, dateTill);
    };

    handleClickTracker = ({tracker_id, type, name, tracker_imei}) => {
        this.props.changeTracker({id: tracker_id, type, name, imei: tracker_imei})
        this.props.fetchTrackerLastPoint(tracker_id)
    };

    handleToggleVisibility = (key) => {
        this.setState(prevState => ({
            [key]: !prevState[key]
        }))
    }

    render() {
        const {classes, authenticated, user, tracker} = this.props;
        const {isVisibleHeader, isVisibleParking} = this.state;
        const {activeTrackerData, activeTracker, trackDetails, lastPoint, activeTrackerParking} = tracker

        //console.log('>>>>>>>>>>>>>>>>>>> state', this.state);

        const trackerList = tracker.result ?
            <TrackerList>
                {
                    tracker.result.map(el => (
                        <li key={el.tracker_id}
                            className={el.tracker_id === activeTracker.id ? 'active' : ''}
                            onClick={() => this.handleClickTracker(el)}
                        >
                            {el.name}
                            <img src={icon[el.type]} alt=""/>
                        </li>
                    ))
                }
            </TrackerList>
            : null;

        const trackerDetailsList = activeTracker.id ?
            <TrackerDetailsList>
                        <li key={activeTracker.id}>
                            {activeTracker.name}
                            <img src={icon[activeTracker.type]} alt=""/>
                        </li>
                <li>
                    {`IMEI: `}
                   <span>{activeTracker.imei}</span>
                </li>
                    ))

                {
                    Object.keys(trackDetails)[0] && Object.keys(trackDetails).map(el => (
                        <li key={`${activeTracker}_${el}`}
                        >
                            {`${el}: ${trackDetails[el]}`}
                        </li>
                    ))
                }

                <li>
                    {`parking: `}
                    <span>{activeTrackerParking.length}</span>
                </li>

            </TrackerDetailsList>
            : null;

        return (
            authenticated ? <Wrapper>

                <Header className={isVisibleHeader? '' : 'hidden'}>
                    <div>
                    <span>{user.organization}</span>

                    <form className={classes.container} noValidate action="">
                        <TextField
                            id="dateFrom"
                            label="from"
                            type="datetime-local"
                            defaultValue={moment().startOf('month').format().slice(0, 16)}
                            className={classes.textField}
                            onChange={this.handleInputChange}
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                        <TextField
                            id="dateTill"
                            label="till"
                            type="datetime-local"
                            defaultValue={moment().format().slice(0, 16)}
                            className={classes.textField}
                            onChange={this.handleInputChange}
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </form>

                    <ButtonLogOut onClick={() => this.handleLogOut()}>
                        <img src={exit} alt="logout"/>
                    </ButtonLogOut>

                        <ButtonVisibility onClick={() => this.handleToggleVisibility('isVisibleHeader')} className={isVisibleHeader? '' : 'hidden'}>
                            <img src={down} alt=""/>
                        </ButtonVisibility>
                    </div>
                </Header>
                <Container>

                    <SideBar>
                        {trackerList}
                        <TrackerDetails>
                            <BlockButtons>
                                <SmallButton onClick={() => this.handleToggleVisibility('isVisibleParking')}>
                                    <img src={activeTrackerData[0] && isVisibleParking ? parking_active : parking} alt=""/>
                                </SmallButton>
                                <SmallButton onClick={()=>{}}>
                                        <img src={view} alt=""/>
                                </SmallButton>
                                <SmallButton onClick={()=>{}}>
                                    <img src={diagram} alt=""/>
                                </SmallButton>
                                <SmallButton type="submit" onClick={this.onSubmit}>
                                        <img src={!activeTrackerData[0] ? route : route_active} alt=""/>
                                </SmallButton>

                            </BlockButtons>
                            <span>Details</span>
                            {trackerDetailsList}
                        </TrackerDetails>
                    </SideBar>

                    <Main>
                        <MyMapComponent
                            activeTracker={activeTracker}
                            data={activeTrackerData}
                            lastPoint={{...lastPoint}}
                            parking={isVisibleParking ? activeTrackerParking : []}
                            icon={iconTop[activeTracker.type]}
                        />

                    </Main>

                </Container>

            </Wrapper> : null
        )
    }
}

const mapStateToProps = state => ({
    authenticated: state.session.authenticated,
    user: state.session.user,
    tracker: state.tracker,
    location: state.router.location,
});

const mapDispatch = (dispatch) => ({
    fetchTrackers: () => dispatch(fetchTrackers()),
    logUserOut: () => dispatch(logUserOut()),
    changeTracker: (trackerInfo) => dispatch(changeTracker(trackerInfo)),
    fetchTrackerLastPoint: (trackerId) => dispatch(fetchTrackerLastPoint(trackerId)),
    fetchTrackerData: (trackerId, from, till) => dispatch(fetchTrackerData(trackerId, from, till)),
});

export default compose(
    withStyles(styles),
    withRouter,
    connect(
        mapStateToProps,
        mapDispatch,
    ),
)(Trackers);
