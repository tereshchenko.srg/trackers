import React, {Component} from 'react'
import {compose} from 'redux';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {createMuiTheme, MuiThemeProvider, withStyles} from "@material-ui/core/styles";

import {logUserIn} from 'store/actions';
import styled from "styled-components";

import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Button from "@material-ui/core/Button";

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#898989',
        },
        secondary: {
            main: '#E00051',
        },
        error: {
            main: '#E12020'
        }
    },
});

const styles = {
    body: {
        width: 252,

    },
    form: {
        width: '50%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
    },
    formControl: {
        width: 252,
        '& > label': {
            fontSize: 11,
            fontFamily: 'Roboto',
            fontStyle: 'normal',
            color: '#898989',
        },
        '& > *': {
            color: '#898989!important',
        },
        '& > div': {
            marginTop: '11px',
        },
        '& > div > input': {
            fontSize: 13,
            paddingLeft: 10,
            fontFamily: 'Roboto',
            fontStyle: 'normal',
            width: 252,
            height: 19,
            color: '#898989',
            border: '0',
            outline: 'none',
            '&:focus': {
                color: '#000000!important',
            },
        },
    },
    cssUnderline: {
        '&:before': {
            borderBottom: '1px solid #D8D8D8',
        },
        '&:after': {
            borderBottom: '1px solid #000000',
        },
    },
    action: {
        padding: '0',
        margin: '10px 0 0',
        display: 'flex',
        justifyContent: 'space-between',
        '& > button > span': {
            fontSize: 13,
            fontFamily: 'Roboto',
            fontStyle: 'normal',
            fontWeight: '500',
        }
    },
    textError: {
        color: '#E12020',
        fontSize: 11,
    },
    button: {
        width: 252,
        marginBottom: 20,
        fontSize: 14,
        textTransform: 'none',
    },
    leftIcon: {
        marginRight: 8,
    },
};

const ButtonCancel = styled(Button)`
&:hover {
color: #ffffff!important;
background-color: #C9C9C9!important;
}
`;
const ButtonLogIn = styled(Button)`
&:hover {
color: #ffffff!important;
background-color: #E91E63!important;
}
`;

const Wrapper = styled.div`
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
`;

const INITIAL_STATE = {username: '', password: ''};

class Login extends Component {
    state = {...INITIAL_STATE}


    componentDidMount() {
        this.checkAuthentication()
    }

    componentDidUpdate() {
        this.checkAuthentication()
    }

    checkAuthentication = () => {
        if (this.props.authenticated) {
            this.props.history.push({pathname: `/map`})
        }
    };

    onSubmit = e => {
        e.preventDefault();
        const {username, password} = this.state;

        if (username === '' || password === '') return;

        this.props.logUserIn({'username': this.state.username, 'password': this.state.password});
        this.resetState();
    };

    resetState = () => {
        this.setState({...INITIAL_STATE});
    };

    handleInputChange = ({target}) => {
        const {value, name} = target;

        this.setState({[name]: value});
    };


    render() {
        const {username, password} = this.state;
        const {classes, error} = this.props;

        return (
            <Wrapper>
                <div className={classes.body}>
                    <form action="">
                        {
                            error.error !== null &&
                            <span style={styles.textError}>{error.non_field_errors && error.non_field_errors[0]}</span>
                        }
                        <FormControl
                            className={classes.formControl}
                            error={error.error !== null && true}
                            aria-describedby="component-error-text"
                        >
                            <InputLabel
                                htmlFor="component-error"
                            >
                                Email
                            </InputLabel>
                            <Input
                                id={error.error !== null ? 'error' : '1'}
                                type="email"
                                name="username"
                                value={username}
                                onChange={this.handleInputChange}
                                classes={{
                                    underline: classes.cssUnderline,
                                }}
                            />
                            {error.error !== null &&
                            <FormHelperText id="component-error-text">
                                {error.error}
                            </FormHelperText>
                            }
                        </FormControl>

                        <FormControl
                            className={classes.formControl}
                            error={error.error !== null && true}
                            aria-describedby="component-error-text"
                        >
                            <InputLabel
                                htmlFor="component-error"
                            >
                                Password
                            </InputLabel>
                            <Input
                                id={error.error !== null ? 'error' : '2'}
                                type="password"
                                name="password"
                                value={password}
                                onChange={this.handleInputChange}
                                classes={{
                                    underline: classes.cssUnderline,
                                }}
                            />
                            {error !== null &&
                            <FormHelperText id="component-error-text">
                                {error.error}
                            </FormHelperText>
                            }
                        </FormControl>

                    </form>
                    <div className={classes.action}>
                        <MuiThemeProvider theme={theme}>
                            <ButtonLogIn type="submit" onClick={this.onSubmit} color="secondary">
                                Login
                            </ButtonLogIn>
                            <ButtonCancel onClick={() => this.resetState()} color="primary">
                                Cancel
                            </ButtonCancel>
                        </MuiThemeProvider>
                    </div>
                </div>
            </Wrapper>
        )
    }
}

const mapStateToProps = state => ({
    authenticated: state.session.authenticated,
    error: state.session.error,
    location: state.router.location,
});
const mapDispatchToProps = {logUserIn};

export default compose(
    withStyles(styles),
    withRouter,
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
)(Login);
